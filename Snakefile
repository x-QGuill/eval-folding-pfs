PATH_HERE="/home/qguilloteau/eval-folding-pfs"
RESULT_FOLDER=f"{PATH_HERE}/data"

NIX_COMPANION_FILE=f"{PATH_HERE}/nix-user-chroot.sh"

FLAVOURS = [

    # "g5k-nfs-store",
    # "g5k-ramdisk"
    "g5k-image"

    # "vm-ramdisk"
]

NB_NODES = [
    2,
    3,
    4,
    5,
    6,
    7,
    8
    # 8,
    # 16,
    # 32
]


rule all:
    input:
        expand(["nxc/build/composition::{file}"], file=FLAVOURS),
        expand(["{result_folder}/results_ior_{nb_nodes}_{flavour}.json"], result_folder=RESULT_FOLDER, flavour=FLAVOURS, nb_nodes=NB_NODES)

rule build_nxc_image:
    input:
        "nxc/flake.nix",
        "nxc/flake.lock",
        "nxc/composition.nix",
        "nxc/my_scripts.nix",
        "nxc/script_ior.nix"
    output:
        "nxc/build/composition::{flavour}"
    shell:
        "cd nxc; nix develop --command nxc build -f {wildcards.flavour}"



rule run_ior:
    input:
        "nxc/build/composition::{flavour}"
    output:
        "{RESULT_FOLDER}/results_ior_{nb_nodes}_{flavour}.json"
    shell:
        "cd nxc; nix develop --command python3 script.py --nxc_build_file {PATH_HERE}/{input} --nb_nodes {wildcards.nb_nodes} --result_dir {RESULT_FOLDER} --flavour {wildcards.flavour}"
