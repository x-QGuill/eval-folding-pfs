{
  description = "A very basic flake";

  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-21.11";
  };

  outputs = { self, nixpkgs }:
  let
    system = "x86_64-linux";
    pkgs = import nixpkgs { inherit system; };

    rPkgs = with pkgs.rPackages; [
      tidyverse
    ];

    myR = pkgs.rWrapper.override { packages = rPkgs; };
    myRstudio = pkgs.rstudioWrapper.override { packages = rPkgs; };
  in
  {
    devShells.${system} = {
      default = pkgs.mkShell {
        buildInputs = [ pkgs.graphviz pkgs.snakemake ];
      };

      rshell = pkgs.mkShell {
        buildInputs = [ myR ];
      };
    };
  };
}

