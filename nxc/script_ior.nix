{ pkgs, setup, ... }:
let
    numTasks = setup.params.numTasks;
    nfsMountPoint = setup.params.nfsMountPoint;
in
''
IOR START
    api=POSIX
    testFile=testFile
    hintsFileName=hintsFile
    multiFile=0
    interTestDelay=5
    readFile=1
    writeFile=1
    filePerProc=0
    checkWrite=0
    checkRead=0
    keepFile=1
    quitOnError=0
    outlierThreshold=0
    setAlignment=1
    singleXferAttempt=0
    individualDataSets=0
    verbose=0
    collective=0
    preallocate=0
    useFileView=0
    keepFileWithError=0
    setTimeStampSignature=0
    useSharedFilePointer=0
    useStridedDatatype=0
    uniqueDir=0
    fsync=0
    storeFileOffset=0
    maxTimeDuration=60
    deadlineForStonewalling=0
    useExistingTestFile=0
    useO_DIRECT=0
    showHints=0

    repetitions=10
    numTasks=${builtins.toString numTasks}
    segmentCount=4
    blockSize=128M
    transferSize=4M

    summaryFile=${nfsMountPoint}/results_ior.json
    summaryFormat=JSON
RUN
IOR STOP
''
