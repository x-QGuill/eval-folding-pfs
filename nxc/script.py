from nixos_compose.nxc_execo import get_oar_job_nodes_nxc, build_nxc_execo

from execo import Process, SshProcess, Remote
from execo_g5k import oardel, oarsub, OarSubmission, get_oar_job_nodes
from execo_engine import Engine, logger, ParamSweeper, sweep

import sys
import os


class MyEngine(Engine):
    def __init__(self):
        super(MyEngine, self).__init__()
        parser = self.args_parser
        parser.add_argument('--nxc_build_file', help='Path to the NXC deploy file')
        parser.add_argument('--nb_nodes', help='Number of nodes')
        parser.add_argument('--walltime', help='walltime in hours')
        parser.add_argument('--result_dir', help='where to store results')
        parser.add_argument('--flavour', help='Flavour')
        self.nodes = {}
        self.oar_job_id = -1
        self.nb_nodes = -1
        self.flavour = None

    def init(self):
        self.nb_nodes = int(self.args.nb_nodes) if self.args.nb_nodes else 2
        walltime_hours = float(self.args.walltime) if self.args.walltime else 1
        nxc_build_file = self.args.nxc_build_file
        self.flavour = self.args.flavour if self.args.flavour else "g5k-image"


        site = "grenoble"
        cluster = "dahu"

        oar_job = reserve_nodes(self.nb_nodes, site, cluster, "deploy" if self.flavour == "g5k-image" else "allow_classic_ssh",  walltime=walltime_hours*60*60)
        self.oar_job_id, site = oar_job[0]

        roles_quantities = {"server": 1, "node": self.nb_nodes - 1}

        self.nodes = get_oar_job_nodes_nxc(
            self.oar_job_id,
            site,
            flavour_name=self.flavour,
            compose_info_file=nxc_build_file,
            roles_quantities=roles_quantities)
        print(self.nodes)

    def run(self):
        result_dir = self.args.result_dir if self.args.result_dir else os.getcwd()

        # Run IOR
        run_ior_remote = Remote("start_ior", self.nodes["node"][0], connection_params={'user': 'root'})
        run_ior_remote.run()

        # Get the result file back

        get_file_command = f"cp /srv/shared/results_ior.json {result_dir}/results_ior_{self.nb_nodes}_{self.flavour}.json"
        get_file_remote = Remote(get_file_command, self.nodes["server"], connection_params={'user': 'root'})
        get_file_remote.run()

def reserve_nodes(nb_nodes, site, cluster, job_type, walltime=3600):
    jobs = oarsub([(OarSubmission("{{cluster='{}'}}/nodes={}".format(cluster, nb_nodes), walltime, job_type=[job_type]), site)])
    return jobs

if __name__ == "__main__":
    ENGINE = MyEngine()
    try:
        ENGINE.start()
    except Exception as ex:
        print(f"Failing with error {ex}")
    oardel([(ENGINE.oar_job_id, None)])
    print("Giving back the resources")
