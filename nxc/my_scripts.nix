{ pkgs, setup, ... }:
let
  # We define some constants here
  nfsMountPoint = setup.params.nfsMountPoint;
  numTasks = setup.params.numTasks;
  iorConfig = "/etc/ior_script";
in {
  # This function takes the number of compute nodes,
  # creates a hostfile for MPI and runs the benchmark
  start_ior =
    pkgs.writeScriptBin "start_ior" ''
      cd ${nfsMountPoint}

      NB_NODES=$(cat /etc/hosts | grep node | wc -l)
      NB_SLOTS_PER_NODE=$((${builtins.toString numTasks} / $NB_NODES))

      cat /etc/hosts | grep node | awk -v nb_slots="$NB_SLOTS_PER_NODE" '{ print $2 " slots=" nb_slots;}' > my_hosts

      mpirun --allow-run-as-root --oversubscribe -mca btl self,vader -np ${builtins.toString numTasks} --hostfile my_hosts ior -f ${iorConfig}
    '';
}
